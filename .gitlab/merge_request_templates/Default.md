### Check list
#### Breaking changes?
- [ ] No
- [ ] Yes (please, describe)

#### How is it tested:
- [ ] Manually
- [ ] New unit / integration checks
- [ ] Covered by existing tests
- [ ] Not required

#### For QA:
- [ ] Described in JIRA task what to test and how
- [ ] Not required

#### Other checks
- [ ] Swagger docs updated if necessary
- [ ] Any new system variables are documented
- [ ] Any dependent changes are merged